import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import '../public/style/index.css'
import "amfe-flexible";
import "@/components/vant"
import "@/mock/mockServe";
import * as API from '@/api';
// 引入vue-lazyload图片懒加载
import VueLazyload from 'vue-lazyload';
// 导入懒加载图片
import errorimage from '@/assets/error.gif'
import loadimage from '@/assets/loading.gif'
// 使用图片懒加载插件
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: errorimage,
  loading: loadimage,
  attempt: 1
})
Vue.config.productionTip = false;

new Vue({
  beforeCreate() {
    Vue.prototype.$bus = this
    Vue.prototype.$api = API // 挂载到原型链上
  },
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
