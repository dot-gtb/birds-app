import axios from "axios";
import nprogress from "nprogress";
import "nprogress/nprogress.css";
import store from "@/store";
const requests = axios.create({
  baseURL: "/api",
  timeout: 5000,
});
requests.interceptors.request.use(
  (config) => {
    if (store.state.detail.uuid_token) {
      config.headers.userTempId = store.state.detail.uuid_token;
    }
    if (store.state.user.token) {
      config.headers.token = store.state.user.token;
    }
    nprogress.start();
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
requests.interceptors.response.use(
  (response) => {
    nprogress.done();
    return response.data;
  },
  (error) => {
    return Promise.reject(error);
  }
);
/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get(url, params) {
  return new Promise((resolve, reject) => {
    requests
      .get(url, {
        params: params,
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err.data);
      });
  });
}

/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function post(url, params) {
  return new Promise((resolve, reject) => {
    requests
      .post(url, params)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err.data);
      });
  });
}
export default requests;
