import mockRequests from "@/api/mockAjax";
import requests from "@/api/request";
export const reqBirdsList = (id) => mockRequests.get(`/birds/${id}`);
export const reqUserInfo = (id) => mockRequests.get(`/user/${id}`);
export const reqBirdInfo = (id) => mockRequests.get(`/bird/${id}`);
export const reqSearchInfo = ( data ) => requests({
    method: 'post',
    url: '/list',
    data
});
export const reqLogin = (data) => requests({
    method: 'post',
    url: '/user/passport/login',
    data
})
export const reqMyOrderList = ({ page = 1, limit = 5 })=>requests.get(`/order/auth/${ page }/${ limit }`);