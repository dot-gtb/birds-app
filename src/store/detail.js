import { reqBirdsList, reqBirdInfo } from "@/api";
import Vue from "vue";
export default {
  state: {
    birdsList: [],
    birdInfo: {},
  },
  mutations: {
    SETBIRDLIST(state, birdsList) {
      state.birdsList = birdsList;
    },
    SETBIRDIFO(state, birdInfo) {
      state.birdInfo = birdInfo;
    },
  },
  actions: {
    async getBirdsList({ commit }, id) {
      try {
        const res = await reqBirdsList(id);
        console.log(res);
        if (res.code === 200) {
          commit("SETBIRDLIST", res.data);
        }
      } catch (error) {
        Vue.$toast(error || "获取列表失败");
      }
    },
    async getBirdInfo({ commit }, id) {
        console.log(id)
      try {
        const res = await reqBirdInfo(id);
        console.log(res);
        if (res.code === 200) {
          commit("SETBIRDIFO", res.data);
        }
      } catch (error) {
        Vue.$toast(error || "获取详情失败");
      }
    },
  },
  namespaced: true,
};
