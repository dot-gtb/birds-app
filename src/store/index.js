import Vue from 'vue'
import Vuex from 'vuex'
import { reqUserInfo } from '@/api';
import detail from './detail'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    userInfo:{},
  },
  getters: {
  },
  mutations: {
    SETUSERINFO(state, userInfo) {
      state.userInfo = userInfo
    },
  },
  actions: {
    async getUserInfo({ commit }, id) {
      try {
        const res = await reqUserInfo(id)
        if (res.code === 200) {
            commit('SETBIRDLIST', res.data)
        }
       } catch (error) {
        this.$toast(error || '获取用户信息失败')
       }
    }
  },
  modules: {
    detail
  }
})
