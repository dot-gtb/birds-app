import Vue from "vue";
import { Button } from 'vant';
import { Field } from 'vant';
import { Toast } from 'vant';
import { Icon } from 'vant';
import { Image as VanImage } from 'vant';
import { Search } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { ShareSheet } from 'vant';
import { ImagePreview } from 'vant';
Vue.use(ShareSheet);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Search);
Vue.use(VanImage);
Vue.use(Icon);
Vue.use(Field);
Vue.use(Button);
Vue.prototype.$toast = Toast
Vue.prototype.$imagePreview = ImagePreview