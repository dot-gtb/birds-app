import Mock from "mockjs";
import birdsData from "./birds_data.json"
import userData from "./userInfo.json"
Mock.mock(/\/mock\/birds\/[1-9]\d*$/,(config)=> {
    const pathArr = config.url.split('/')
    const id = pathArr[pathArr.length - 1]
    console.log(birdsData)
    const data = birdsData.filter( item => item.id == id)[0].items
    return {code: 200, data }
})
Mock.mock(/\/mock\/bird\/[1-9]\d*$/,(config)=> {
    const pathArr = config.url.split('/')
    const id = pathArr[pathArr.length - 1]
    console.log(birdsData)
    const data = birdsData[0].items.filter( item => item.id == id)[0]
    return {code: 200, data }
})

Mock.mock(/\/mock\/user\/[1-9]\d*$/,(config)=> {
    const pathArr = config.url.split('/')
    const id = pathArr[pathArr.length - 1]
    const userInfo = userData.filter( item => item.id == id)[0]
    return {code: 200, data: userInfo }
})
