// 生成随机游客id
// 引入uuid随机字符串生成器
import { v4 as uuidv4 } from 'uuid';
export default function getUUID() {
    let uuid_token = localStorage.getItem('UUIDTOKEN')
    if (!uuid_token) {
        uuid_token = uuidv4()
        localStorage.setItem('UUIDTOKEN', uuid_token)
    }
    return uuid_token
}
