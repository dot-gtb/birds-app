import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '@/views/Home/HomeView'
Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login/LoginView')
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/Detail/DetailView')
  },
  {
    path: '/my',
    name: 'my',
    component: () => import('../views/My/MyView')
  },
  {
    path: '/shopping',
    name: 'shopping',
    component: () => import('../views/Shopping/ShoppingView')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Register/RegisterView')
  },
]
const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
