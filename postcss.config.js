// postcss.config.js
module.exports = {
    plugins: {
        'postcss-pxtorem': {
            rootValue: 37.5,  // 此设计稿尺寸为375px
            propList: ['*'],
        },
    },
};
